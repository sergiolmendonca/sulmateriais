package br.com.materiais.sul.sulmateriais

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_valores.*

class valores : AppCompatActivity() {

    //DADOS: NOME, CIMENTO, CAL, AREIA FINA, AREIA MÉDIA, BRITA 1, TEM FRETE?
    var valor1 = arrayOf("Marom", "Aliança", "Zanetti")
    var valor2 = arrayOf(34.95, 36.82, 34.88) //cimento
    var valor3 = arrayOf(13, 11.40, 10.85) //cal
    var valor4 = arrayOf(149, 119, 126) //areia fina
    var valor5 = arrayOf(124, 119, 116) //areia media
    var valor6 = arrayOf(124, 129, 116) //brita 1
    var valor7 = arrayOf("não", "não", "sim") // frete?*//*



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_valores)


        //
        tv11.setText("")
        tv12.setText(valor1[0])
        tv13.setText(valor1[1])
        tv14.setText(valor1[2])
        //
        tv21.setText("Cimento")
        tv22.setText(valor2[0].toString())
        tv23.setText(valor2[1].toString())
        tv24.setText(valor2[2].toString())
        //
        tv31.setText("Cal")
        tv32.setText(valor3[0].toString())
        tv33.setText(valor3[1].toString())
        tv34.setText(valor3[2].toString())
        //
        tv41.setText("Areia Fina")
        tv42.setText(valor4[0].toString())
        tv43.setText(valor4[1].toString())
        tv44.setText(valor4[2].toString())
        //
        tv51.setText("Areia Média")
        tv52.setText(valor5[0].toString())
        tv53.setText(valor5[1].toString())
        tv54.setText(valor5[2].toString())
        //
        tv61.setText("Brita 1")
        tv62.setText(valor6[0].toString())
        tv63.setText(valor6[1].toString())
        tv64.setText(valor6[2].toString())
        //
        tv71.setText("Frete Gratis?")
        tv72.setText(valor7[0])
        tv73.setText(valor7[1])
        tv74.setText(valor7[2])



    }
}


