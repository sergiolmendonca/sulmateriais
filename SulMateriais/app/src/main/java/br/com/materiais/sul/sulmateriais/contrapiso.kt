package br.com.materiais.sul.sulmateriais
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import kotlinx.android.synthetic.main.activity_contrapiso.*

class contrapiso : AppCompatActivity() {

    internal lateinit var sptraco: Spinner
    internal lateinit var opcao: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contrapiso)


        var materiaisSaidaFinal = "Aguardando"

        //Matriz Traços (nome, cimento,areia media)
        var traco1 = arrayOf("Traço 1:3 ",7.6,0.83)
        var traco2 = arrayOf("Traço 1:4 ",6.10,.88)
        var traco3 = arrayOf("Traço 1:5 ",5.11,0.92)

        var espessura:Double = 4.0

        var fatorCimento = traco1[1].toString().toDouble()
        var fatorAreia =  traco1[2].toString().toDouble()


        sptraco = findViewById<ImageView>(R.id.spTraco) as Spinner
        val traco = arrayOf(traco1[0],traco2[0],traco3[0])
        val adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,traco)

        sptraco.adapter = adapter
        sptraco.setSelection(1)
        sptraco.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (sptraco.selectedItem.toString()) {
                    traco1[0].toString() -> {
                        fatorCimento = traco1[1].toString().toDouble()
                        fatorAreia = traco1[2].toString().toDouble()
                    }
                    traco2[0].toString() -> {
                        fatorCimento = traco2[1].toString().toDouble()
                        fatorAreia = traco2[2].toString().toDouble()
                    }
                    traco3[0].toString() -> {
                        fatorCimento = traco3[1].toString().toDouble()
                        fatorAreia = traco3[2].toString().toDouble()
                    }
                }

            }
        }
        opcao = findViewById<ImageView>(R.id.spOpcao) as Spinner
        val espessa = arrayOf("3 cm","4 cm","5 cm")
        val adapter2 = ArrayAdapter(this,android.R.layout.simple_list_item_1,espessa)
        opcao.adapter = adapter2
        opcao.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(opcao.selectedItem.toString()){
                    "3 cm" -> {
                        espessura = 1.5
                    }
                    "4 cm" -> {
                        espessura = 2.0
                    }
                    "5 cm" -> {
                        espessura = 2.5
                    }

                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }


        }
        btShare2.setOnClickListener{
            val pIntent = Intent(Intent.ACTION_SEND)
            pIntent.type = "text/plain"
            pIntent.putExtra(Intent.EXTRA_TEXT,  materiaisSaidaFinal)
            startActivity(Intent.createChooser(pIntent,"Compartilhar com:"))
        }
        btCalConcreto.setOnClickListener {

            if(camVolume.text.toString() == ""){
                camSaida.setText("Por favor digitar numero válido")
            } else {
                btShare2.visibility = View.VISIBLE

                var valor = camVolume.text.toString()
                var valorD = valor.toDouble()*espessura/100
                var cimento = "%.2f".format(valorD * fatorCimento)
                var areia = "%.2f".format(valorD * fatorAreia)
                var brita = "%.2f".format(valorD)

                var cimentoValor = Math.floor(valorD * fatorCimento * 32)
                var areiaValor = Math.floor(valorD * fatorAreia * 100)
                var britaValor = Math.floor(valorD* 110)


                var matTotal = areiaValor + cimentoValor

                materiaisSaidaFinal = "---Materiais para Contrapiso---\n Cimento: $cimento Uni. (50kg) -  R$ $cimentoValor  \n Areia Média: $areia m³  -  R$ $areiaValor \n Camada de Brita:$brita m³ -  R\$ $britaValor " +
                        " Total Geral: R$ $matTotal"

                camSaida.setText(materiaisSaidaFinal)
            }
        }

    }}
