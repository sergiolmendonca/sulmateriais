package br.com.materiais.sul.sulmateriais

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_concreto.*

class concreto : AppCompatActivity() {

    internal lateinit var opcao:Spinner


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_concreto)


        var materiaisSaidaFinal = "Aguardando"

        //Matriz Traços (nome, cimento, areia, brita e agua)
        val traco1 = arrayOf("Traço 1:2:3 (25Mpa)",6.88,0.486,0.728)
        val traco2 = arrayOf("Traço 1:2,5:3 (23Mpa)",6.38,0.562,0.674)
        val traco3 = arrayOf("Traço 1:3:3 (17Mpa)",6.00,0.620,0.620)
        val traco4 = arrayOf("Traço 1:5:5 (5Mpa)",5.80,0.700,0.700)

        var fatorCimento = traco1[1].toString().toDouble()
        var fatorAreia =  traco1[2].toString().toDouble()
        var fatorBrita = traco1[3].toString().toDouble()

        opcao = findViewById<ImageView>(R.id.spOpcao) as Spinner
        val traco = arrayOf(traco1[0],traco2[0],traco3[0],traco4[0])
        val adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,traco)
        opcao.adapter = adapter
        opcao.setSelection(2)
        opcao.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(opcao.selectedItem.toString()){
                    traco1[0].toString() -> {
                        fatorCimento = traco1[1].toString().toDouble()
                        fatorAreia =  traco1[2].toString().toDouble()
                        fatorBrita = traco1[3].toString().toDouble()
                    }
                    traco2[0].toString() -> {
                        fatorCimento = traco2[1].toString().toDouble()
                        fatorAreia =  traco2[2].toString().toDouble()
                        fatorBrita = traco2[3].toString().toDouble()
                    }
                    traco3[0].toString() -> {
                        fatorCimento = traco3[1].toString().toDouble()
                        fatorAreia =  traco3[2].toString().toDouble()
                        fatorBrita = traco3[3].toString().toDouble()
                    }
                    traco4[0].toString() -> {
                        fatorCimento = traco4[1].toString().toDouble()
                        fatorAreia =  traco4[2].toString().toDouble()
                        fatorBrita = traco4[3].toString().toDouble()
                    }
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }


        }
        btShare.setOnClickListener{
            val pIntent = Intent(Intent.ACTION_SEND)
            pIntent.type = "text/plain"
            pIntent.putExtra(Intent.EXTRA_TEXT,  materiaisSaidaFinal)
            startActivity(Intent.createChooser(pIntent,"Compartilhar com:"))
        }
        btCalConcreto.setOnClickListener {

            if(camVolume.text.toString() == ""){
                camSaida.setText("Por favor digitar numero válido")
            } else {
                btShare.visibility = View.VISIBLE
                "%.2f".format(5.2323232)

                var valor = camVolume.text.toString()
                var valorD = valor.toDouble()
                var cimento = "%.2f".format(valorD * fatorCimento)
                var areia = "%.2f".format(valorD * fatorAreia)
                var brita = "%.2f".format(valorD * fatorBrita)

                var areiaValor = Math.floor(valorD * fatorAreia * 100)
                var britaValor = Math.floor(valorD * fatorBrita * 110)
                var cimentoValor = Math.floor(valorD * fatorCimento * 32)
                var matTotal = areiaValor + britaValor + cimentoValor

                materiaisSaidaFinal = "---Materiais para Concreto---\n Cimento: $cimento Uni  -  R\$ $cimentoValor \n Areia Média: $areia m³ -  R$ $areiaValor  \n Brita 1:  $brita m³      -  R$ $britaValor \n " +
                        " \n Total Geral: R$ $matTotal"

                camSaida.setText(materiaisSaidaFinal)
            }
        }
    }
}
