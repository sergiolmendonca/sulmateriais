package br.com.materiais.sul.sulmateriais
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_principal.*
import org.jetbrains.anko.toast

class principal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
         toast("Seja bem Vindo!")

         btConcreto.setOnClickListener {
             val intent = Intent(this, concreto::class.java)
             startActivity(intent)
         }
         btReboco.setOnClickListener {
             val intent = Intent(this, reboco::class.java)
             startActivity(intent)
         }
         btContrapiso.setOnClickListener {
             val intent = Intent(this, contrapiso::class.java)
             startActivity(intent)
         }
         btValores.setOnClickListener {
             val intent = Intent(this, valores::class.java)
             startActivity(intent)
         }
         btContato.setOnClickListener {
             val intent = Intent(this, contato::class.java)
             startActivity(intent)
         }
         btOutrosCalculos.setOnClickListener {
             val intent = Intent(this, outrosValores::class.java)
             startActivity(intent)
         }
    }
}
