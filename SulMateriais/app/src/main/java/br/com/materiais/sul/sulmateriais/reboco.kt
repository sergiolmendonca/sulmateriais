package br.com.materiais.sul.sulmateriais

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_concreto.*

class reboco : AppCompatActivity() {


    internal lateinit var sptraco: Spinner
    internal lateinit var opcao: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reboco)


        var materiaisSaidaFinal = "Aguardando"

        //Matriz Traços (nome, cimento,cal, areia fina)
        var traco1 = arrayOf("Traço 1:2:5 ",4.00,20.00,0.80)
        var traco2 = arrayOf("Traço 1:2:6 ",3.50,16.00,0.728)
        var traco3 = arrayOf("Traço 1:2:7 ",3.00,14.00,0.728)
        var traco4 = arrayOf("Traço 1:2:8 ",2.55,12.80,0.91)
        var espessura:Double = 2.0

        var fatorCimento = traco1[1].toString().toDouble()
        var fatorCal =  traco1[2].toString().toDouble()
        var fatorAreiaF = traco1[3].toString().toDouble()

        sptraco = findViewById<ImageView>(R.id.spTraco) as Spinner
        val traco = arrayOf(traco1[0],traco2[0],traco3[0],traco4[0])
        val adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,traco)

        sptraco.adapter = adapter
        sptraco.setSelection(1)
        sptraco.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (sptraco.selectedItem.toString()) {
                    traco1[0].toString() -> {
                        fatorCimento = traco1[1].toString().toDouble()
                        fatorCal = traco1[2].toString().toDouble()
                        fatorAreiaF = traco1[3].toString().toDouble()
                    }
                    traco2[0].toString() -> {
                        fatorCimento = traco2[1].toString().toDouble()
                        fatorCal = traco2[2].toString().toDouble()
                        fatorAreiaF = traco2[3].toString().toDouble()
                    }
                    traco3[0].toString() -> {
                        fatorCimento = traco3[1].toString().toDouble()
                        fatorCal = traco3[2].toString().toDouble()
                        fatorAreiaF = traco3[3].toString().toDouble()
                    }
                    traco4[0].toString() -> {
                        fatorCimento = traco4[1].toString().toDouble()
                        fatorCal = traco4[2].toString().toDouble()
                        fatorAreiaF = traco4[3].toString().toDouble()
                    }

                }

            }
        }
        opcao = findViewById<ImageView>(R.id.spOpcao) as Spinner
        val espessa = arrayOf("1,5 cm","2,0 cm","2,5 cm","3,0 cm")
        val adapter2 = ArrayAdapter(this,android.R.layout.simple_list_item_1,espessa)
        opcao.adapter = adapter2
        opcao.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(opcao.selectedItem.toString()){
                    "1,5 cm" -> {
                        espessura = 1.5
                    }
                    "2,0 cm" -> {
                        espessura = 2.0
                    }
                    "2,5 cm" -> {
                        espessura = 2.5
                    }
                    "3,0 cm" -> {
                        espessura = 3.0
                    }


                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }


        }
        btShare.setOnClickListener{
            val pIntent = Intent(Intent.ACTION_SEND)
            pIntent.type = "text/plain"
            pIntent.putExtra(Intent.EXTRA_TEXT,  materiaisSaidaFinal)
            startActivity(Intent.createChooser(pIntent,"Compartilhar com:"))
        }
        btCalConcreto.setOnClickListener {

            if(camVolume.text.toString() == ""){
                camSaida.setText("Por favor digitar numero válido")
            } else {
                btShare.visibility = View.VISIBLE

                var valor = camVolume.text.toString()
                var valorD = valor.toDouble()*espessura/100
                var cimento = "%.2f".format(valorD * fatorCimento)
                var cal = "%.2f".format(valorD * fatorCal)
                var areiaF = "%.2f".format(valorD * fatorAreiaF)

                var cimentoValor = Math.floor(valorD * fatorCimento * 32)
                var calValor = Math.floor(valorD * fatorCal * 9)
                var areiaFValor = Math.floor(valorD * fatorAreiaF * 120)


                var matTotal = areiaFValor + calValor + cimentoValor

                materiaisSaidaFinal = "---Materiais para Reboco---\n Cimento: $cimento Uni. (50kg) -  R$ $cimentoValor  \n Cal Hidr.:  $cal Unid.  -  R$ $calValor \n Areia Fina:    $areiaF m³  -  R$ $areiaFValor \n" +
                        " Total Geral: R$ $matTotal"

                camSaida.setText(materiaisSaidaFinal)

            }
        }
    }


}
